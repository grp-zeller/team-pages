# People

<!-- text all on one line please or else Wordpress gets confused about line endings :( -->

## Georg Zeller


## Toby Hodges

GitHub: [tobyhodges](https://github.com/tobyhodges)  
Twitter: [@tbyhdgs](https://twitter.com/tbyhdgs)  
ORCID: [orcid.org/0000-0003-1766-456X](https://orcid.org/0000-0003-1766-456X)

<img alt='Toby Hodges' src='https://git.embl.de/grp-zeller/team-pages/images/Toby_circular.png'
style='float:right;padding-left:10px' width='200px'>
Toby is a staff bioinformatician whose main responsibilities are in coordinating the [Bio-IT Project](https://bio-it.embl.de). He has a PhD in bioinformatics from the University of York, is a certified [Software Carpentry](http://software-carpentry.org) instructor, an organiser for the [HUB series](http://hub-hub.de), a punk rock fan, and a keen cyclist.

## Alessio Milanese

<img alt='Alessio Milanese' src='https://git.embl.de/grp-zeller/team-pages/images/AlessioMilanese_circular.png' 
style='float:right;padding-left:10px' width='200px'>
Alessio joined the group as a PhD fellow in September 2016. He obtained his Bachelor and Master degree in Bioinformatics and Medical Biotechnology at the University of Verona (Italy). After nine months as research assistant at the University of Zurich (Switzerland), he joined the Zeller team. His research aims to improve the characterization of microbial communities, developing new tools for the analysis of their taxonomic composition and functional activity.

## Konrad Zych
website: [konradzych.pl/](http://konradzych.pl/)  
Twitter: [@KonradZych](https://twitter.com/KonradZych)  
ORCID: [orcid.org/0000-0001-7426-0516](https://orcid.org/0000-0001-7426-0516)

Konrad is a Postdoctoral fellow responsible for maintaining and extending our toolset as well as analysis of microbiome data. He has a PhD (in spe) in bioinformatics (from University of Groningen). He is a member of [HD-HuB](https://www.hd-hub.de/), scientific posters ninja, PADI Advanced Open Water diver and an avid traveller.

## Jakob Wirbel

GitHub: [jakob-wirbel](https://github.com/jakob-wirbel)  
Twitter: [@JakobWirbel](https://twitter.com/JakobWirbel)  
ORCID: [orcid.org/0000-0002-4073-3562](http://orcid.org/0000-0002-4073-3562)

Jakob started in August 2017 as a PhD student in the Zeller group. He studied Molecular Biotechnology at the University of Heidelberg with short stops in Montreal, Cambridge, and Aachen. In his research, he is interested in advancing the interpretation of microbiome data in health and disease by modeling confounding factors and developing new statistical methods.

## Jonas Fleck

GitHub: [astair](https://github.com/astair)
Twitter: [@JonasSimonFleck](https://twitter.com/JonasSimonFleck)

Jonas is a Master’s student at the University of Heidelberg. During his studies in Heidelberg, Sydney, Glasgow and Stockholm, he explored various areas of biological research before he settled for bioinformatics. Currently, he is combining assembly with read partitioning approaches to improve the analysis of large and complex microbiome datasets. Also, he likes climbing things.
