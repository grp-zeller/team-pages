# Markdown Files for Team Webpages

- Place images in the `images` directory
- add/edit your biography in `biographies.md`
  - Name as header
  - Markdown image syntax is insufficient here. Place circular* images in the `img` 
  tags (copy from a pre-existing bio), change the `alt` and `src` attributes, 
  leave everything else as-is. 
  - Pp to six points for ORCID, Google Scholar, Twitter, GitHub etc
  - ~50 word text
- research page content can be edited/updated via `research.md`

\* Use the script [here](https://git.embl.de/stamper/circularize-images/blob/master/circularize.py) to circularize images. talk to Toby if you need to do 
something more elaborate